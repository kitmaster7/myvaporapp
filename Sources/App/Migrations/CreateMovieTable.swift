//
//  File.swift
//  
//
//  Created by Alex on 13.07.2020.
//

import Foundation
import Fluent
import FluentPostgresDriver

struct CreateMovieTableMigration: Migration{
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        database.schema("movies")
            .id()
            .field("title", .string)
            .create()
    }
    
    
    //undo
    func revert(on database: Database) -> EventLoopFuture<Void> {
        database.schema("movies")
            .delete()
    }
    

}
